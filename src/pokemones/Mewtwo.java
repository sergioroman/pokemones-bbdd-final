package pokemones;

/**
 *
 * @author Luis Casanova, María Segura y Sergio Román
 */
public class Mewtwo extends Pokemon implements Pokemontura{

    /**
     * constructor mewtwo
     * @param nivel el nivel del pokemon que viene dado random
     */
    public Mewtwo(int nivel) {
        this.nivel = nivel;
        this.tipo = new String[1];
        this.tipo[0] = "Psíquico";
        this.ps = nivel * 60;
        this.ataque = nivel * 80;
        this.defensa = nivel * 90;
    }
    
    /**
     * método para montar en el pokemon
     * @return te has montado en el pokemon mewtwo
     */
    @Override
    public String montar() {
        return "Ya te has montado en MewTwo y a cabalgar!!!!";
    }

   
}
