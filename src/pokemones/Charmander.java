package pokemones;

/**
 *
 * @author Luis Casanova, María Segura y Sergio Román
 */
public class Charmander extends Pokemon implements Pokemontura{

    /**
     * constructor charmander
     * @param nivel el nivel del pokemon que viene dado random
     */
    public Charmander(int nivel) {
        this.nivel = nivel;
        this.tipo = new String[1];
        this.tipo[0] = "Fuego";
        this.ps = nivel * 80;
        this.ataque = nivel * 100;
        this.defensa = nivel * 90;
    }
    /**
     * método para montar en el pokemon
     * @return te has montado en el pokemon charmander
     */
    @Override
    public String montar() {
       return "Ya te has montado en Charmander, cuando evolucione volarás";
    }

}
