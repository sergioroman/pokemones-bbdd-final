package pokemones;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

/**
 *
 * @author Sergio Román
 */
public class JuegoDePokemon {

    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);
        menu(teclado);
    }

    public static void menu(Scanner teclado) {
        Mochila mochila = new Mochila();
        int opcion = 3;
        boolean error = true;
        while (opcion != 0) {
            System.out.println("MENU");
            System.out.println("=====");
            System.out.println("1) Capturar pokemon");
            System.out.println("2) Listar pokemon");
            System.out.println("0) Salir");
            error = true;
            while (error) {
                try {
                    opcion = teclado.nextInt();
                    error = false;
                } catch (java.util.InputMismatchException ex) {
                    System.out.println("Has metido letras, vuelve a intentarlo");
                    teclado.next();
                }
            }
            switch (opcion) {
                case 1: {
                    mochila = capturarPokemon(mochila, teclado);

                    String q = "INSERT INTO pokemon (nombre, nivel, tipo, ps, ataque, defensa) VALUES (?,?,?,?,?,?)";
                    Connection dbConnection;

                    try {
                        dbConnection = DriverManager.getConnection(
                                "jdbc:mysql://localhost/" + "pokemonbbdd" + "?serverTimezone=UTC", "root", "asdf1234");

                        PreparedStatement preparedStatement = dbConnection.prepareStatement(q);

                        preparedStatement = dbConnection.prepareStatement(q);

                        Pokemon ultimoPokemon = mochila.getPokemon(mochila.getCantidad() - 1);

                        preparedStatement.setString(1, ultimoPokemon.getNombre());
                        preparedStatement.setInt(2, ultimoPokemon.getNivel());
                        preparedStatement.setString(3, ultimoPokemon.getTipo()[0]);
                        preparedStatement.setInt(4, ultimoPokemon.getPs());
                        preparedStatement.setInt(5, ultimoPokemon.getAtaque());
                        preparedStatement.setInt(6, ultimoPokemon.getDefensa());
                        preparedStatement.executeUpdate();

                        System.out.println("Pokemon añadidos");

                    } catch (SQLException e) {
                        System.out.println(e.getMessage());

                    }

                    break;
                }
                case 2: {
                    if (mochila.getCantidad() == 0) {
                        System.out.println("Todavía no tienes ningún pokemon en la mochila");
                    } else {

                        String db = "pokemonbbdd";
                        Connection con;
                        Statement query;
                        ResultSet result;
                        try {
                            con = DriverManager.getConnection(
                                    "jdbc:mysql://localhost/" + db + "?serverTimezone=UTC", "root", "asdf1234");
                            System.out.println("Conexion establecida");
                            query = con.createStatement();
                            System.out.println("query creada");
                            result = query.executeQuery("SELECT * FROM pokemon");
                            System.out.println("Tus pokemones son:");

                            while (result.next()) {
                                System.out.print(result.getString("nombre") + " ");
                                System.out.print(result.getInt("nivel") + " ");
                                System.out.print(result.getString("tipo") + " ");
                                System.out.print(result.getString("ps") + " ");
                                System.out.print(result.getInt("ataque") + " ");
                                System.out.println(result.getInt("defensa") + " ");
                            }

                            result.close();
                            query.close();
                            con.close();

                        } catch (SQLException ex) {
                            System.out.println("Error en la conexion con " + db + ": " + ex.toString());
                        }

                    }
                    break;
                }

                case 0: {
                    break;
                }
                default: {
                    System.out.println("Seleccione una opción correcta");
                }
            }
        }

    }

    public static Mochila capturarPokemon(Mochila auxiliar, Scanner teclado) {
        String SiONo;
        String mote;
        Pokemon poke;
        int especie, nivel;
        especie = (int) (Math.random() * 6);
        nivel = (int) (Math.random() * 100 + 1);
        switch (especie) {
            case 0: {
                Pikachu pica = new Pikachu(nivel);
                poke = pica;
                break;
            }
            case 1: {
                Charmander charm = new Charmander(nivel);
                poke = charm;
                break;
            }
            case 2: {
                Squirtle squir = new Squirtle(nivel);
                poke = squir;
                break;
            }
            case 3: {
                Bulbasaur bulb = new Bulbasaur(nivel);
                poke = bulb;
                break;
            }
            case 4: {
                Mew mew = new Mew(nivel);
                poke = mew;
                break;
            }
            default: {
                Mewtwo mewtwo = new Mewtwo(nivel);
                poke = mewtwo;
                break;
            }
        }
        System.out.println("Pokemon Capturado!!");
        System.out.println("===================");
        System.out.println("Especie: " + poke.getClass().getSimpleName());
        System.out.println("Nivel: " + poke.getNivel());
        System.out.println("Mote: ");
        System.out.println("¿Quiere ponerle un nombre?(S/N)");
        SiONo = (teclado.next()).toUpperCase();
        if (SiONo.equals("S")) {
            System.out.println("Introduce el nombre");
            mote = teclado.next();
        } else {
            mote = poke.getClass().getSimpleName();
        }
        poke.setMote(mote);
        try {
            auxiliar.añadirPokemon(poke);
        } catch (MochilaLlenaException ex) {
            System.out.println(ex.toString());
        }
        return auxiliar;

    }

    /**
     * funcion del pokemon capturado
     *
     * @param poke
     * @param teclado
     */
    public static void pokemonCapturado(Pokemon poke, Scanner teclado) {

    }

    public static void listarPokemon(Mochila auxiliar, Scanner teclado) {
        String opcion = "";
        int quePokemon = 0;
        boolean error = true;
        System.out.println("Pokemon");
        System.out.println("=======");
        for (int i = 0; i < auxiliar.getCantidad(); i++) {
            System.out.print(i + 1 + " ");
            System.out.print(auxiliar.getPokemon(i).getNombre());
            System.out.print(" (" + auxiliar.getPokemon(i).getClass().getSimpleName() + ") - lvl ");
            System.out.println(auxiliar.getPokemon(i).getNivel());
        }
        teclado.nextLine();
        while (!opcion.equals("V")) {
            System.out.println("Opciones (M - Mostrar, L - Liberar, V - Volver)");
            opcion = teclado.next().toUpperCase();
            switch (opcion) {
                case "M": {
                    System.out.println("Introduce el número del pokemon");
                    error = true;
                    while (error) {
                        try {
                            quePokemon = teclado.nextInt();
                            error = false;
                        } catch (java.util.InputMismatchException ex) {
                            System.out.println("Has metido letras, vuelve a intentarlo");
                            teclado.next();
                        }
                    }
                    if ((auxiliar.getCantidad() < quePokemon) || (quePokemon == 0)) {
                        System.out.println("Ese pokemon no existe");
                    } else {
                        mostrarPokemon(auxiliar.getPokemon(quePokemon - 1), teclado);
                    }
                    break;
                }
                case "L": {
                    System.out.println("Introduce el número del pokemon");
                    error = true;
                    while (error) {
                        try {
                            quePokemon = teclado.nextInt();
                            error = false;
                        } catch (java.util.InputMismatchException ex) {
                            System.out.println("Has metido letras, vuelve a intentarlo");
                            teclado.next();
                        }
                    }
                    if ((auxiliar.getCantidad() < quePokemon) || (quePokemon == 0)) {
                        System.out.println("Ese pokemon no existe");
                    } else {
                        auxiliar.eliminarPokemon(quePokemon - 1);
                        System.out.println("Pokemon eliminado!!");
                    }
                    break;
                }
                case "V": {
                    break;
                }
                default: {
                }

            }

        }
    }

    /**
     * funcion que muestra el pokemon
     *
     * @param aMostrar clase del pokemon que se muestra
     * @param teclado scanner in por teclado
     */
    public static void mostrarPokemon(Pokemon aMostrar, Scanner teclado) {
        String SiONo;
        System.out.println(aMostrar.getNombre());
        System.out.println("=====================");
        System.out.println("Especie: " + aMostrar.getClass().getSimpleName());
        System.out.println("Nivel: " + aMostrar.getNivel());
        System.out.print("Tipo: ");
        for (int i = 0; i < aMostrar.getTipo().length; i++) {
            System.out.print(" " + aMostrar.getTipo()[i]);
        }
        System.out.println();
        System.out.println("PS: " + aMostrar.getPs());
        System.out.println("Ataque: " + aMostrar.getAtaque());
        System.out.println("Defensa: " + aMostrar.getDefensa());
        Class[] i = aMostrar.getClass().getInterfaces();
        try {
            if (!i[0].equals(null)) {
                System.out.println("¿Te quieres montar en el pokemon?(S/N)");
                SiONo = (teclado.next()).toUpperCase();
                if (SiONo.equals("S")) {
                    System.out.println(((Pokemontura) aMostrar).montar());
                }
            }
        } catch (java.lang.ArrayIndexOutOfBoundsException ex) {
            //Si da la excepcion es porque no implementa la interface
        }

    }
}
