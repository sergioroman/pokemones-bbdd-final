package pokemones;

/**
 *
 * @author Luis Casanova, María Segura y Sergio Román
 */
public class Mew extends Pokemon {

    /**
     * constructor mew
     * @param nivel el nivel del pokemon que viene dado random
     */
    public Mew(int nivel) {
        this.nivel = nivel;
        this.tipo = new String[1];
        this.tipo[0] = "Psíquico";
        this.ps = nivel * 60;
        this.ataque = nivel * 80;
        this.defensa = nivel * 90;
    }
}
