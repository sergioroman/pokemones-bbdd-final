package pokemones;

/**
 *
 * @author Luis Casanova, María Segura y Sergio Román
 */
public class Squirtle extends Pokemon {

    public Squirtle(int nivel) {
        this.nivel = nivel;
        this.tipo = new String[1];
        this.tipo[0] = "Agua";
        this.ps = nivel * 60;
        this.ataque = nivel * 80;
        this.defensa = nivel * 90;
    }
}
