package pokemones;

/**
 *
 * @author Luis Casanova, María Segura y Sergio Román
 */
public class Bulbasaur extends Pokemon implements Pokemontura{

    /**
     * constructor bulbasaur
     * @param nivel el nivel del pokemon que viene dado random
     */
    public Bulbasaur(int nivel) {
        this.nivel = nivel;
        this.tipo = new String[2];
        this.tipo[0] = "Veneno";
        this.tipo[1] = "Planta";
        this.ps = nivel * 60;
        this.ataque = nivel * 80;
        this.defensa = nivel * 90;
    }
    /**
     * método para montar en el pokemon
     * @return te has montado en el pokemon bulbasaur
     */
    @Override
    public String montar() {
        return "Ya te has montado en Bulbasaur, cuidado con su veneno";
    }
}
