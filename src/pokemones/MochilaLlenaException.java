package pokemones;

/**
 *
 * @author Luis Casanova, Maria Segura y Sergio Román
 */
public class MochilaLlenaException extends Exception {
    /**
     * metodo que lanza la excepcion extendida de Exception
     * @return un String con mensaje de mochila llena
     */
    @Override
    public String toString() {
        return "Tienes la mochila llena, para capturar más pokemon deberás liberar alguno";
    }
}
