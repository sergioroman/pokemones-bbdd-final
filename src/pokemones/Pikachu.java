package pokemones;

/**
 *
 * @author Luis Casanova, María Segura y Sergio Román
 */
public class Pikachu extends Pokemon {

    public Pikachu(int nivel) {
        this.nivel = nivel;
        this.tipo = new String[1];
        this.tipo[0] = "Eléctrico";
        this.ps = nivel * 60;
        this.ataque = nivel * 100;
        this.defensa = nivel * 80;
    }

}
