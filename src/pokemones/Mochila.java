package pokemones;

/**
 *
 * @author Sergio Román
 */
public class Mochila {

    private final Pokemon[] pokemons;
    private int cantidad;

    /**
     * constructor mochila
     */
    public Mochila() {
        this.cantidad = 0;
        this.pokemons = new Pokemon[101];
    }

    /**
     * metodo para añadir un pokemon nuevo
     *
     * @param nuevo el pokemon nuevo que añades
     * @throws MochilaLlenaException lanza la excepcion
     */
    public void añadirPokemon(Pokemon nuevo) throws MochilaLlenaException {
        try {
            this.pokemons[this.cantidad] = nuevo;
            this.cantidad++;
        } catch (java.lang.ArrayIndexOutOfBoundsException ex) {
            throw new MochilaLlenaException();
        }
    }

    /**
     * metodo para eliminar un pokemon
     *
     * @param posicion elimina la posicion del pokemon que ocupa
     */
    public void eliminarPokemon(int posicion) {
        for (int i = posicion; i < this.cantidad; i++) {
            this.pokemons[i] = this.pokemons[i + 1];
        }
        this.cantidad = this.cantidad - 1;
    }

    /**
     * metodo que devuelve el pokemon que esta en esa posicion
     *
     * @param posicion de la posicion que ocupa el pokemon en el array
     * @return devuelve la posicion del pokemon en el array
     */
    public Pokemon getPokemon(int posicion) {
        return this.pokemons[posicion];
    }

    public Pokemon[] getPokemones() {
        return this.pokemons;
    }

    /**
     * metodo que devuelve la cantidad de pokemon
     *
     * @return la cantidad de pokemon
     */
    public int getCantidad() {
        return this.cantidad;
    }
}
