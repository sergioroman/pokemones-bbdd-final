package pokemones;

/**
 *
 * @author Luis Casanova, María Segura y Sergio Román
 */
public class Pokemon {

    protected String nombre;
    protected int nivel;
    protected String tipo[];
    protected int ps;
    protected int ataque;
    protected int defensa;

    /**
     * metodo get del nombre
     * @return el nombre del pokemon
     */
    public String getNombre() {
        return this.nombre;
    }
    
    /**
     * metodo get del nivel
     * @return el nivel del pokemon
     */
    public int getNivel() {
        return this.nivel;
    }
    
    /**
     * metodo get del tipo que es un array
     * @return el tipo del pokemon
     */
    public String[] getTipo() {
        return this.tipo;
    }

    /**
     * metodo get de la vida
     * @return la vida del pokemon
     */
    public int getPs() {
        return this.ps;
    }
    
    /**
     * metodo get del ataque
     * @return el ataque del pokemon
     */
    public int getAtaque() {
        return this.ataque;
    }
    
    /**
     * metodo get de la defensa
     * @return la defensa del pokemon
     */
    public int getDefensa() {
        return this.defensa;
    }

    /**
     * metodo set del mote
     * @return el mote del pokemon
     */
    public void setMote(String mote) {
        this.nombre = mote;
    }
}
