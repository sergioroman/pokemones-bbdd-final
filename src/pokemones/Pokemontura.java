package pokemones;

/**
 *
 * @author Luis Casanova, Maria Segura y Sergio Román
 */
public interface Pokemontura {

    /**
     * metodo get de la montura
     * @return si se ha montado en la montura
     */
    public String montar();
}
